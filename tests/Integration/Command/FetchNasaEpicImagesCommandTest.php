<?php

namespace App\Tests\Integration\Command;

use App\Command\FetchNasaEpicImagesCommand;
use App\Service\NasaApi\Epic\EpicApiInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Filesystem\Filesystem;

class FetchNasaEpicImagesCommandTest extends KernelTestCase
{
    /**
     * @var CommandTester
     */
    private CommandTester $commandTester;
    /**
     * @var EpicApiInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    private EpicApiInterface $epicApiClient;

    public function setUp(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $this->epicApiClient = $this->createMock(EpicApiInterface::class);
        $filesystem = $this->createMock(Filesystem::class);

        $imageMetadataItem = $this->getImageMetadataItem();

        $this->epicApiClient
            ->method('getMetadata')
            ->willReturn([$imageMetadataItem]);

        $application->add(new FetchNasaEpicImagesCommand($this->epicApiClient, $filesystem));

        $command = $application->find('app:fetch-nasa-images');
        $this->commandTester = new CommandTester($command);

    }

    /**
     * Testing that command is successfully executed with both arguments
     *
     */
    public function testExecuteCommandWithAllArgsSuccessfully()
    {
        $this->commandTester->execute([
            'target_directory' => 'public/nasa',
            'date' => '08-04-2022',
        ]);

        $this->commandTester->assertCommandIsSuccessful();
    }

    /**
     * Testing that command is successfully executed with only target_directory argument
     *
     */
    public function testExecuteCommandWithoutDateSuccessfully()
    {
        $this->commandTester->execute([
            'target_directory' => 'public/nasa',
        ]);

        $this->commandTester->assertCommandIsSuccessful();
    }

    /**
     * Testing that command is invalid if wrong date format is provided
     *
     */
    public function testExecuteCommandWithInvalidDate()
    {
        $this->commandTester->execute([
            'target_directory' => 'public/nasa',
            'date' => '08/04/2022',
        ]);

        $this->assertEquals(Command::INVALID, $this->commandTester->getStatusCode());
        $this->assertStringContainsString('Date invalid. Make sure you provide date in d-m-Y format (10-04-2022).', $this->commandTester->getDisplay());
    }

    /**
     * Testing that different target dir paths
     *
     */
    public function testExecuteCommandWithDifferentPaths()
    {
        $targetDirs = [
            [
                'targetDir' => '../public/',
                'expectedStatusCode' => Command::SUCCESS,
            ],
            [
                'targetDir' => 'public/nasa',
                'expectedStatusCode' => Command::SUCCESS,
            ],
            [
                'targetDir' => 'public/../nasa',
                'expectedStatusCode' => Command::SUCCESS,
            ],
            [
                'targetDir' => '/var/html/www/plenigo_nasa_task/public',
                'expectedStatusCode' => Command::SUCCESS,
            ],
            [
                'targetDir' => 'public\\\nasa',
                'expectedStatusCode' => Command::SUCCESS,
            ],
        ];

        foreach ($targetDirs as $targetDir) {
            $this->testPath($targetDir['targetDir'], $targetDir['expectedStatusCode']);
        }

    }

    /**
     * Method for testing one target dir path
     *
     * @param $targetDir
     * @param $expectedStatusCode
     */
    private function testPath($targetDir, $expectedStatusCode)
    {
        $this->commandTester->execute([
            'target_directory' => $targetDir,
        ]);

        $this->assertEquals($this->commandTester->getStatusCode(), $expectedStatusCode);
    }

    /**
     * Testing if images are already downloaded that we ask
     * if they would like to download again or not
     *
     */
    public function testOutputForSameDate()
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $filesystem = $this->createMock(Filesystem::class);

        $filesystem->expects($this->exactly(2))
            ->method('exists')
            ->willReturnOnConsecutiveCalls(false, true);

        $application->add(new FetchNasaEpicImagesCommand($this->epicApiClient, $filesystem));

        $command = $application->find('app:fetch-nasa-images');
        $this->commandTester = new CommandTester($command);

        $this->commandTester->execute([
            'target_directory' => 'public/nasa',
            'date' => '08-04-2022',
        ]);

        $this->commandTester->execute([
            'target_directory' => 'public/nasa',
            'date' => '08-04-2022',
        ]);

        $this->assertStringContainsString('You already downloaded images for this date.', $this->commandTester->getDisplay());
        $this->assertStringContainsString('Would you like to download them again?', $this->commandTester->getDisplay());
    }

    /**
     * Creating stdClass for one Metadata Image item
     *
     * @return \stdClass
     */
    private function getImageMetadataItem()
    {
        $imageMetadata = new \stdClass();
        $imageMetadata->identifier = '20220411005515';
        $imageMetadata->caption = 'This image was taken by NASA\'s EPIC camera onboard the NOAA DSCOVR spacecraft';
        $imageMetadata->image = 'epic_1b_20220411005515';
        $imageMetadata->version = '03';
        $imageMetadata->date = '2022-04-11 00:50:27';

        return $imageMetadata;
    }
}