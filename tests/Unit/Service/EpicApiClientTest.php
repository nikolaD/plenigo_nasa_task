<?php

namespace App\Tests\Unit\Service;

use App\Service\NasaApi\Enums\EpicApi;
use App\Service\NasaApi\Epic\EpicApiClient;
use App\Service\NasaApi\Exceptions\NasaConfigException;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class EpicApiClientTest extends TestCase
{
    /**
     * @var HttpClientInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    protected HttpClientInterface $httpClient;

    protected function setUp(): void
    {
        parent::setUp();
        $this->httpClient = $this->createMock(HttpClientInterface::class);
    }

    /**
     * Testing fetching image metadata successfully
     *
     * @throws TransportExceptionInterface
     */
    public function testGetMetadataSuccessful()
    {
        $epicApiClient = new EpicApiClient($this->httpClient, 'https://test.com/', 'apiKey');
        $response = $this->createMock(ResponseInterface::class);

        $this->httpClient
            ->expects($this->once())
            ->method('request')
            ->with('GET', EpicApi::ENDPOINT_PREFIX->value . '/natural')
            ->willReturn($response);

        $response
            ->expects($this->once())
            ->method('getContent')
            ->willReturn('[]');

        $data = $epicApiClient->getMetadata();
        $this->assertEquals([], $data);

    }

    /**
     * Testing epic image path is correct
     *
     */
    public function testImagePathFormat()
    {
        $apiBaseUri = 'https://test.com/';
        $apiKey = 'apiKey';
        $year = '2022';
        $month = '04';
        $day = '13';
        $imageName = 'epic.png';

        $epicApiClient = new EpicApiClient($this->httpClient, $apiBaseUri, $apiKey);
        $expected = sprintf(EpicApi::EPIC_IMAGE_PATH->value, $apiBaseUri, $year, $month, $day, $imageName, $apiKey);

        $this->assertEquals($expected, $epicApiClient->getImagePath($year, $month, $day, $imageName));
    }

    /**
     * Testing that NasaConfigException is thrown if $apiBaseURI and $apiKey are not provided
     *
     * @throws TransportExceptionInterface
     */
    public function testConfigurationException()
    {
        $epicApiClient = new EpicApiClient($this->httpClient, '', '');

        $this->expectException(NasaConfigException::class);

        $epicApiClient->getMetadata();
    }

    /**
     * Testing that Not found exception is thrown
     *
     * @throws TransportExceptionInterface
     */
    public function testClientNotFoundException()
    {
        $epicApiClient = new EpicApiClient($this->httpClient, 'https://test.com/', 'apiKey');
        $response = $this->createMock(ResponseInterface::class);
        $clientException = $this->createMock(ClientExceptionInterface::class);

        $this->httpClient
            ->expects($this->once())
            ->method('request')
            ->with('GET', EpicApi::ENDPOINT_PREFIX->value . '/natural')
            ->willReturn($response);

        $response
            ->expects($this->once())
            ->method('getContent')
            ->will($this->throwException($clientException));

        $response
            ->expects($this->once())
            ->method('getStatusCode')
            ->willReturn(404);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Not found');

        $epicApiClient->getMetadata();
    }

    /**
     * Testing that Client default exception is thrown
     *
     * @throws TransportExceptionInterface
     */
    public function testClientDefaultException()
    {
        $epicApiClient = new EpicApiClient($this->httpClient, 'https://test.com/', 'apiKey');
        $response = $this->createMock(ResponseInterface::class);
        $clientException = $this->createMock(ClientExceptionInterface::class);

        $this->httpClient
            ->expects($this->once())
            ->method('request')
            ->with('GET', EpicApi::ENDPOINT_PREFIX->value . '/natural')
            ->willReturn($response);

        $response
            ->expects($this->once())
            ->method('getContent')
            ->will($this->throwException($clientException));

        $response
            ->expects($this->once())
            ->method('getStatusCode')
            ->willReturn(500);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Something wrong happened while trying to communicate with NASA API');

        $epicApiClient->getMetadata();
    }

    /**
     * Testing that Transport Exception is thrown
     *
     * @throws TransportExceptionInterface
     */
    public function testTransportException()
    {
        $epicApiClient = new EpicApiClient($this->httpClient, 'https://test.com/', 'apiKey');
        $response = $this->createMock(ResponseInterface::class);
        $transportException = $this->createMock(TransportExceptionInterface::class);

        $this->httpClient
            ->expects($this->once())
            ->method('request')
            ->with('GET', EpicApi::ENDPOINT_PREFIX->value . '/natural')
            ->willReturn($response);

        $response
            ->expects($this->once())
            ->method('getContent')
            ->will($this->throwException($transportException));

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Something wrong happened while trying to communicate with NASA API');

        $epicApiClient->getMetadata();
    }
}