# Plenigo Nasa Epic Images Task #

## The task:

We need to have a command that takes all images of a given day from the NASA EPIC API and puts them into a given folder.
We want the command to accept at least two parameters:
- date (optional), last available day if empty
- target folder
  
The command should create a subfolder (of target folder) per date to store images to.

### Project Setup
Nothing "fancy" is used here, just plane symfony project with only requirement to use `PHP >= 8.1` since Enums are used in this project https://www.php.net/manual/en/language.enumerations.php
```bash
composer install
```
### Running the command ###
If [date] argument is not provided latest available images will be downloaded
```bash
php bin/console app:fetch-nasa-images public/nasa 11-04-2022
```
#### Output
```bash
Downloading image epic_1b_20220411005515.png 1 of 13...
Downloading image epic_1b_20220411024317.png 2 of 13...
Downloading image epic_1b_20220411043119.png 3 of 13...
Downloading image epic_1b_20220411061922.png 4 of 13...
Downloading image epic_1b_20220411080724.png 5 of 13...
Downloading image epic_1b_20220411095526.png 6 of 13...
Downloading image epic_1b_20220411114328.png 7 of 13...
Downloading image epic_1b_20220411133130.png 8 of 13...
Downloading image epic_1b_20220411151932.png 9 of 13...
Downloading image epic_1b_20220411170735.png 10 of 13...
Downloading image epic_1b_20220411185537.png 11 of 13...
Downloading image epic_1b_20220411204339.png 12 of 13...
Downloading image epic_1b_20220411223142.png 13 of 13...
Images successfully saved at public/nasa/2022/04/11
```
### Running tests ###

```bash
php bin/phpunit
```
### Running php linter ###

```bash
./vendor/bin/phplint ./
```
