<?php

namespace App\Service\NasaApi\Exceptions;

class NasaConfigException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct('Configuration exception NASA_API_KEY and/or NASA_API_BASE_URI are missing.');
    }
}
