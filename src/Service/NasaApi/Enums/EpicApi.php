<?php

namespace App\Service\NasaApi\Enums;

enum EpicApi: string
{
    case ENDPOINT_PREFIX = 'EPIC/api';
    case DATE_FORMAT = 'Y-m-d';
    case EPIC_IMAGE_PATH = '%sEPIC/archive/natural/%s/%s/%s/png/%s?api_key=%s';
}