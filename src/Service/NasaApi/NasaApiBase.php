<?php

namespace App\Service\NasaApi;

use App\Service\NasaApi\Enums\EpicApi;
use App\Service\NasaApi\Exceptions\NasaConfigException;
use DateTimeInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class NasaApiBase
{
    /**
     * @var HttpClientInterface
     */
    protected HttpClientInterface $httpClient;
    /**
     * @var string
     */
    protected string $apiBaseURI;
    /**
     * @var string
     */
    protected string $apiKey;

    public function __construct(HttpClientInterface $httpClient, string $apiBaseURI, string $apiKey)
    {
        $this->httpClient = $httpClient;
        $this->apiBaseURI = $apiBaseURI;
        $this->apiKey = $apiKey;
    }
}