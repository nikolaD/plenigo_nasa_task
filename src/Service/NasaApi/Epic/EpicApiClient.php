<?php

namespace App\Service\NasaApi\Epic;

use App\Service\NasaApi\Enums\EpicApi;
use App\Service\NasaApi\Exceptions\NasaConfigException;
use App\Service\NasaApi\NasaApiBase;
use DateTimeInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class EpicApiClient extends NasaApiBase implements EpicApiInterface
{

    /**
     * Gets images metadata from NASA EPIC Api
     *
     * @param DateTimeInterface|null $date
     * @return array
     * @throws TransportExceptionInterface
     * @throws \Exception
     */
    public function getMetadata(DateTimeInterface $date = null): array
    {
        if(!$this->apiBaseURI || !$this->apiKey) {
            throw new NasaConfigException();
        }

        $endpoint = EpicApi::ENDPOINT_PREFIX->value . '/natural';

        if ($date) {
            $endpoint .= '/date/'.$date->format(EpicApi::DATE_FORMAT->value);
        }

        $response = $this->httpClient->request('GET', $endpoint);

        try {
            return json_decode($response->getContent());
        } catch (ClientExceptionInterface $e) {
            throw match ($response->getStatusCode()) {
                404 => new \Exception('Not found'),
                default => new \Exception('Something wrong happened while trying to communicate with NASA API'),
            };
        } catch (TransportExceptionInterface $e) {
            throw new \Exception('Something wrong happened while trying to communicate with NASA API');
        }
    }

    /**
     * Gets properly formatted image path to NASA EPIC image
     *
     * @param string $year
     * @param string $month
     * @param string $day
     * @param string $imageName
     * @return string
     */
    public function getImagePath(string $year, string $month, string $day, string $imageName): string
    {
        if(!$this->apiBaseURI || !$this->apiKey) {
            throw new NasaConfigException();
        }

        return sprintf(EpicApi::EPIC_IMAGE_PATH->value, $this->apiBaseURI, $year, $month, $day, $imageName, $this->apiKey);
    }

}