<?php

namespace App\Service\NasaApi\Epic;

use DateTimeInterface;

interface EpicApiInterface
{
    public function getMetadata(DateTimeInterface $date = null): array;
    public function getImagePath(string $year, string $month, string $day, string $imageName): string;
}