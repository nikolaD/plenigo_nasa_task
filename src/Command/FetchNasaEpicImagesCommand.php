<?php

namespace App\Command;

use App\Service\NasaApi\Epic\EpicApiInterface;
use App\Service\NasaApi\Exceptions\NasaConfigException;
use DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Path;

class FetchNasaEpicImagesCommand extends Command
{
    private EpicApiInterface $epicApiClient;
    private Filesystem $filesystem;

    protected static $defaultName = 'app:fetch-nasa-images';
    protected static $defaultDescription = 'Fetch\'s all images for given date from NASA EPIC API and stores them in given directory';

    public function __construct(EpicApiInterface $epicApiClient, Filesystem $filesystem)
    {
        $this->epicApiClient = $epicApiClient;
        $this->filesystem = $filesystem;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('target_directory', InputArgument::REQUIRED, 'The target directory where images will be stored.')
            ->addArgument('date', InputArgument::OPTIONAL, 'The date last available day if empty.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $inputDate = $input->getArgument('date');
        $targetDirectory = $input->getArgument('target_directory');
        $date = null;

        if ($inputDate) {
            if (!$date = DateTime::createFromFormat('d-m-Y', $inputDate)) {
                $output->writeln('<error>Date invalid. Make sure you provide date in d-m-Y format (10-04-2022).</error>');

                return Command::INVALID;
            }
        }

        try {
            $metadata = $this->epicApiClient->getMetadata($date);
        } catch (NasaConfigException $e) {
            $output->writeln("<error>{$e->getMessage()}</error>");

            return Command::INVALID;
        }

        if (!$metadata) {
            $output->writeln("<error>Sorry no images for selected date {$inputDate}. Please select another date or leave [date] argument empty to get latest available.</error>");

            return Command::INVALID;
        }

        if(!$inputDate) {
            $date = DateTime::createFromFormat('Y-m-d h:i:s', $metadata[0]->date);
        }

        $year = $date->format('Y');
        $month = $date->format('m');
        $day = $date->format('d');

        $targetDirectory .= '/' . $year . '/' . $month . '/' .$day;

        if ($this->filesystem->exists($targetDirectory)) {
            $output->writeln('<comment>You already downloaded images for this date.</comment>');

            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion('<question>Would you like to download them again?</question>', false);

            if(!$helper->ask($input, $output, $question)) {
                return Command::INVALID;
            }
        }

        try {
            $targetDirectory = Path::canonicalize($targetDirectory);
            $this->filesystem->mkdir(
                $targetDirectory,
            );
        } catch (IOExceptionInterface $exception) {
            $output->writeln("<error>An error occurred while creating your directory at {$exception->getPath()}</error>");
        }

        $imagesCount = count($metadata);
        $i = 1;

        foreach($metadata as $image) {
            $name = $image->image . '.png';

            $output->writeln("<info>Downloading image {$name} {$i} of {$imagesCount}...</info>");

            $source = $this->epicApiClient->getImagePath($year, $month, $day, $name);

            $destination = $targetDirectory . '/' . $name;

            try {
                $this->filesystem->copy($source, $destination);
            } catch (FileNotFoundException $e) {
                $output->writeln("<error>{$e->getMessage()}</error>");

                return Command::INVALID;
            } catch (IOExceptionInterface $e) {
                $output->writeln("<error>{$e->getMessage()}</error>");

                return Command::INVALID;
            }


            $i++;
        }
        $output->writeln("<info>Images successfully saved at {$targetDirectory}.</info>");

        return Command::SUCCESS;
    }
}